﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;
using System;

namespace gitproj
{
   class ProjectEventsHandler : IVsHierarchyEvents
   {
      private IdeCore _ide;

      internal ProjectEventsHandler(IdeCore ide)
      {
         _ide = ide;
      }

      public int OnInvalidateIcon(IntPtr hicon)
      {
         _ide.printLine("IVsHierarchyEvents.OnInvalidateIcon");
         return VSConstants.S_OK;
      }

      public int OnInvalidateItems(uint itemidParent)
      {
         _ide.printLine("IVsHierarchyEvents.OnInvalidateItems");
         return VSConstants.S_OK;
      }

      public int OnItemAdded(uint itemidParent, uint itemidSiblingPrev, uint itemidAdded)
      {
         _ide.printLine("IVsHierarchyEvents.OnItemAdded");
         return VSConstants.S_OK;
      }

      public int OnItemDeleted(uint itemid)
      {
         _ide.printLine("IVsHierarchyEvents.OnItemDeleted");
         return VSConstants.S_OK;
      }

      public int OnItemsAppended(uint itemidParent)
      {
         _ide.printLine("IVsHierarchyEvents.OnItemsAppended");
         return VSConstants.S_OK;
      }

      public int OnPropertyChanged(uint itemid, int propid, uint flags)
      {
         _ide.printLine("IVsHierarchyEvents.OnPropertyChanged " + hierarchyPropertyToString(propid));
         return VSConstants.S_OK;
      }

      string hierarchyPropertyToString(int propid)
      {
         return 
            toEnumName<__VSHPROPID>(propid) ?? 
            toEnumName<__VSHPROPID2>(propid) ?? 
            toEnumName<__VSHPROPID3>(propid) ?? 
            toEnumName<__VSHPROPID4>(propid) ??
            toEnumName<__VSHPROPID5>(propid) ?? 
            toEnumName<__VSHPROPID6>(propid) ??
            propid.ToString();
      }

      string toEnumName<T>(int propid)
      {
         if (Enum.IsDefined(typeof(T), propid))
         {
            T id = (T)(object)propid;
            return id.ToString();
         }
         return null;
      }
   }
}
