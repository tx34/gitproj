﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace gitproj
{
   class MenuItem
   {
      public string display;
      public string command;
   }

   class PathSettings : JsonSettings<PathSettings>
   {
      public string relativeSrc = "sw";
      public MenuItem[] variants =
      {
         new MenuItem{ display=@"sw", command=@"@runi cd $proj\sw" }, 
         new MenuItem{ display=@".build\debug", command=@"@runp cd $src\.build\debug\$top" }, 
         new MenuItem{ display=@".build\release", command=@"@copy cd $src\.build\release\$top" }
      };
      public string environment = @"C:\project\environment.bat";
   }

   public class JsonSettings<T> where T : new()
   {
      public void save(string fileName)
      {
         var serializer = new JavaScriptSerializer();
         File.WriteAllText(fileName, serializer.Serialize(this));
      }

      public static T load(string fileName)
      {
         if (File.Exists(fileName))
         {
            var serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(File.ReadAllText(fileName));
         }
         return new T();
      }
   }
}