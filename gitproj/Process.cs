﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace gitproj
{
   class ProcessOutput
   {
      public string output { get; private set; } 
      public string error { get; private set; }
      public int exitCode { get; private set; }
      public const int EXIT_TIMEOUT = 124;
      public const int EXIT_ERROR = 1;

      public ProcessOutput(string output, string error, int exitCode)
      {
         this.output = output;
         this.error = error;
         this.exitCode = exitCode;
      }
   }

   // Do not just wait for exit, if the buffer is full it will block
   class Process
   {
      public static void runAsyncCommand(string command, string directory, Action<string> outputAction)
      {
         System.Diagnostics.Process process = new System.Diagnostics.Process();
         process.StartInfo = hiddenInfo(command, directory);

         process.OutputDataReceived += (sender, e) =>
         {
            if (e.Data != null)
            {
               outputAction(e.Data);
            }
         };
         process.ErrorDataReceived += (sender, e) =>
         {
            if (e.Data != null)
            {
               outputAction(e.Data);
            }
         };

         try
         {
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
         }
         catch (Exception e)
         {
            outputAction(commandError(command, directory, e));
         }
      }

      private static string commandError(string command, string directory, Exception e)
      {
         return "Error running \"" + command + "\" in " + directory + " - " + e.Message;
      }

      public static ProcessOutput runBlockingCommand(string command, string directory)
      {
         System.Diagnostics.Process process = new System.Diagnostics.Process();
         process.StartInfo = hiddenInfo(command, directory);
         StringBuilder output = new StringBuilder();
         StringBuilder error = new StringBuilder();
         using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
         using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
         {
            process.OutputDataReceived += (sender, e) =>
            {
               if (e.Data == null)
               {
                  outputWaitHandle.Set();
               }
               else
               {
                  output.AppendLine(e.Data);
               }
            };
            process.ErrorDataReceived += (sender, e) =>
            {
               if (e.Data == null)
               {
                  errorWaitHandle.Set();
               }
               else
               {
                  error.AppendLine(e.Data);
               }
            };

            try
            {
               process.Start();
               process.BeginOutputReadLine();
               process.BeginErrorReadLine();
            }
            catch (Exception e)
            {
               return new ProcessOutput("", commandError(command, directory, e), ProcessOutput.EXIT_ERROR);
            }

            int timeout = 30000;
            if (process.WaitForExit(timeout) && outputWaitHandle.WaitOne(timeout) && errorWaitHandle.WaitOne(timeout))
            {
               return new ProcessOutput(output.ToString(), error.ToString(), process.ExitCode);
            }
            else
            {
               return new ProcessOutput(output.ToString(), error.ToString(), ProcessOutput.EXIT_TIMEOUT);
            }
         }
      }

      private static System.Diagnostics.ProcessStartInfo hiddenInfo(string command, string directory)
      {
         var startInfo = new System.Diagnostics.ProcessStartInfo();
         startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
         startInfo.CreateNoWindow = true;
         startInfo.FileName = "cmd.exe";
         startInfo.Arguments = "/c " + command;
         startInfo.RedirectStandardOutput = true;
         startInfo.RedirectStandardError = true;
         // otherwise fails with Git\mingw64/libexec/git-core\git-submodule: line 330: 0: Bad file descriptor
         startInfo.RedirectStandardInput = true;
         startInfo.UseShellExecute = false;
         startInfo.WorkingDirectory = directory;
         return startInfo;
      }

      public static void runInteractiveCommand(string command, string directory, Action<string> errorAction)
      {
         var startInfo = new System.Diagnostics.ProcessStartInfo();
         startInfo.FileName = "cmd.exe";
         startInfo.Arguments = "/k" + command;
         startInfo.UseShellExecute = false;
         startInfo.WorkingDirectory = directory;
         System.Diagnostics.Process process = new System.Diagnostics.Process();
         process.StartInfo = startInfo;
         try
         {
            process.Start();
         }
         catch(Exception e)
         {
            errorAction(commandError(command, directory, e));
         }
      }

      private IdeCore _ide;

      public Process(IdeCore ide)
      {
         _ide = ide;
      }

      public List<string> scrapeEnvironment(string path, string pattern)
      {
         var envFile = new FileInfo(path);
         ProcessOutput process = runBlockingCommand(envFile.Name + " && set", envFile.DirectoryName);
         printError(process);
         var valueList = new List<string>();
         var reader = new StringReader(process.output);
         while(reader.Peek() >= 0)
         {
            string entry = reader.ReadLine();
            string[] values = entry.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
            if(values.Length == 2 && values[0].Contains(pattern))
            {
               valueList.Add(values[1]);
            }
         }
         return valueList;
      }

      public void printError(ProcessOutput process)
      {
         if(process.exitCode != 0)
         {
            if(process.error.EndsWith("\n"))
            {
                _ide.print(process.error);         
            }
            else
            {
               _ide.printLine(process.error);
            }
         }
      }

      public string readOutput(ProcessOutput process)
      {
         printError(process);
         return process.output.TrimEnd('\n');
      }
   }
}
