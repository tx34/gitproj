﻿/*
Copyright (c) 2018 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System.Xml;

namespace gitproj
{
   /*
   // SelectNodes does not work without this when xmlns is defined
   if (doc.DocumentElement.Attributes["xmlns"] != null)
   {
       string xmlns = doc.DocumentElement.Attributes["xmlns"].Value;
       XmlNamespaceManager namespace = new XmlNamespaceManager(doc.NameTable);
       namespace.AddNamespace("MsBuild", xmlns);
       nodes = doc.SelectNodes("/MsBuild:Project/MsBuild:ItemGroup/*", namespace);
   }
   */
   class ProjectXml
   {
      private string _path;
      private XmlNode _other;
      private XmlNode _source;
      private XmlNode _header;
      private XmlDocument _doc;

      public ProjectXml(string path)
      {
         // may need XmlReader/XmlWriter for large files
         _path = path;
         _doc = new XmlDocument();
         _doc.Load(_path);
         _other = null;
         foreach (XmlNode node in _doc.DocumentElement.ChildNodes)
         {
            // an empty project starts with one empty ItemGroup
            if(node.Name == "ItemGroup" && node.ChildNodes.Count == 0)
            {
               _other = node;
            }
         }
         _source = createGroup(_other);
         _header = createGroup(_source);
      }

      public void save()
      {
         _doc.Save(_path);
      }

      public void addFromFile(string file)
      {
         if (file.EndsWith(".h"))
         {
            addHeader(file);
         }
         else if(file.EndsWith(".cxx") || file.EndsWith(".cpp") || file.EndsWith(".cu"))
         {
            addSource(file);
         }
         else
         {
            addOther(file);
         }
      }

      private XmlNode createGroup(XmlNode after)
      {
         XmlElement group = _doc.CreateElement("ItemGroup", _doc.DocumentElement.NamespaceURI);
         _doc.DocumentElement.InsertAfter(group, after); 
         return group;
      }

      private void addOther(string file)
      {
         // there are other item groups like text and xml, does ignoring these mess up Project.ProjectItems manipulation?
         addFile(_other, "None", file);
      }

      private void addSource(string file)
      {
         addFile(_source, "ClCompile", file);
      }      
      
      private void addHeader(string file)
      {
         addFile(_header, "ClInclude", file);
      }

      private void addFile(XmlNode group, string groupHeader, string file)
      {
         XmlElement element = _doc.CreateElement(groupHeader, _doc.DocumentElement.NamespaceURI);
         element.SetAttribute("Include", file);
         group.AppendChild(element);
      }
   }
}
