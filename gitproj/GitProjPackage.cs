﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace gitproj
{
   // https://github.com/Microsoft/VSSDK-Extensibility-Samples
   // The minimum requirement for a class to be considered a valid package for Visual Studio
   // is to implement the IVsPackage interface and register itself with the shell.
   // This package uses Managed Package Framework (MPF)
   // WPF resources require "Resource" build action, resx is used by Windows Forms not WPF
   [PackageRegistration(UseManagedResourcesOnly = true)]
   [InstalledProductRegistration("#110", "#112", "0.1", IconResourceID = 400)] // Info on this package for Help/About
   [ProvideMenuResource("Menus.ctmenu", 1)]
   [Guid(GitProjPackage.PackageGuidString)]
   [ProvideBindingPath]
   [ProvideAutoLoad(UIContextGuids.NoSolution)]
   [ProvideAutoLoad(UIContextGuids.SolutionExists)]
   public sealed class GitProjPackage : Package
   {
      public const string PackageGuidString = "79aad9b7-6847-4487-86a0-3572f332f183";
      
      private IdeCore _ide;
      private GitOperations _git;
      private CreateGitProjCommand _createProjectCommand;
      private RefreshProjectCommand _refreshCommand;
      private CurrentPathCommand _currentPathCommand;
      
      public GitProjPackage() {}

      // Has access to VisualStudio services here.
      protected override void Initialize()
      {
         base.Initialize();

         _ide = new IdeCore(this);
         _git = new GitOperations(_ide);

         _createProjectCommand = new CreateGitProjCommand(_ide, _git);
         _refreshCommand = new RefreshProjectCommand(_ide, _git);
         _currentPathCommand = new CurrentPathCommand(_ide, _git);

         _ide.printLine("Initialized " + packageDllPath());
      }

      private string packageDllPath()
      {
         return Assembly.localPath(typeof(GitProjPackage));
      }
   }
}
