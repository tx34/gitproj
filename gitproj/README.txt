gitproj is a Visual Studio extension to syncrhonize a vcxproj with files tracked in git
Ensure git is installed and available on the %PATH%
To create a new project go to "File > New > C++ Project From Git..." and choose the root directory of an existing git repository
Click "Refresh from Git" on the far right of the Solution Explorer to synchronize files and scrape include paths from the config
gitproj.json in the project root has an environment entry that holds the path to a bat file which is executed and scraped for keys that contain "INCLUDE"
The refresh operation does an incremental update to limit disruption to intellisense but is slower than recreating the solution

gitproj can also be used to run up to 10 arbitrary commands 
This is similar to Tools > External Tools but supports project relative paths for build directories
Commands are defined in gitproj.json in the project root
Right click in an open file to run commands defined in the config
These commands can be hotkeyed from Tools > Options > Environment > Keyboard > EditorContextMenus.CodeWindow.PathCmd0 through PathCmd9
To copy the output of a command to the clipboard "command":"@copy echo foo"
To run a command in a new interactive termal "command":"@runi echo foo"
To run a command and pipe output to a Visual Studio output pane "command":"@runp echo foo"
Several substitutions can be made when using "command":"value"
$proj is the solution path
$src is the solutionPath+relativeSrc
$file is the current file name
$fileNoExt is the current file name without its extension
$env is the environment value
$top is the relative path from relativeSrc to the current file 
To open a terminal in the build directory of the current file "command":"@runi cd $src\\.build\\release\\$top"
To access system variables from environment "command":"@runi $env & call echo %PATH%"