﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;

namespace gitproj
{
   public class Collections
   {
      public static T First<T>(System.Collections.IEnumerable e)
      {
         T first = default(T);
         foreach(T item in e)
         {
            first = item;
            break;
         }
         return first;
      }

      public static HashSet<string> splitNewline(string value)
      {
         return splitNewline(value, (entry) => { return true; }, (entry) => { return entry; });
      }

      public static HashSet<string> splitNewline(string value, Func<string,bool> predicate, Func<string,string> transform)
      {
         var files = new HashSet<string>();
         var reader = new StringReader(value);
         while(reader.Peek() >= 0)
         {
            string entry = reader.ReadLine();
            if(entry != "" && predicate(entry))
            {
               files.Add(transform(entry));
            }
         }
         return files;
      }
   }
}
