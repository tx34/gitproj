﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Text;

namespace gitproj
{
   // Tools options page https://msdn.microsoft.com/en-us/library/bb166195.aspx
   class CurrentPathCommand
   {
      public const int BaseCommandId = 256;
      public static readonly Guid CommandSet = new Guid("37b08084-fb9e-4d0d-ae82-741456b5a4c2");
      private IdeCore _ide;
      private GitOperations _git;
      List<MenuCommand> _commands;
      private FileSystemWatcher _settingsWatcher;

      public CurrentPathCommand(IdeCore ide, GitOperations git)
      {
         _ide = ide;
         _git = git;
         _commands = new List<MenuCommand>();
         _settingsWatcher = new FileSystemWatcher();
         _settingsWatcher.Filter = IdeCore.settingsName;
         // Files edited with Visual Studio require NotifyFilters.FileName to catch changes due to writing saves to temporary files
         _settingsWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName; 
         // This may emit several times for one save
         _settingsWatcher.Changed += refreshCommands;
         _settingsWatcher.Renamed += refreshCommands;

         _ide.DteSolutionEvents.Opened += addCommands;
         _ide.DteSolutionEvents.Opened += beginWatching;
         _ide.DteSolutionEvents.BeforeClosing += removeCommands;
         _ide.DteSolutionEvents.BeforeClosing += stopWatching;
      }

      private void refreshCommands(object sender, FileSystemEventArgs e)
      {
         _ide.printLine("Reloading: " + e.FullPath);
         refreshCommands();
      }

      private void beginWatching()
      {
         _settingsWatcher.Path = _ide.loadedSolutionPath();
         _settingsWatcher.EnableRaisingEvents = true;
      }

      private void stopWatching()
      {
         _settingsWatcher.EnableRaisingEvents = false;
      }

      private void refreshCommands()
      {
         removeCommands();
         addCommands();
      }

      private void removeCommands()
      {
         foreach(var command in _commands)
         {
            _ide.MenuCommandService.RemoveCommand(command);
         }
      }

       // Dynamic menu command https://msdn.microsoft.com/en-us/library/bb166492.aspx
       // Commands with <CommandFlag>DynamicItemStart</CommandFlag> won't end up in command list
      private void addCommands()
      {
         string solutionPath = _ide.loadedSolutionPath();
         PathSettings pathSettings = _ide.settings();
         string srcPath = Path.Combine(solutionPath, pathSettings.relativeSrc);
         for(int i = 0; i < pathSettings.variants.Length; ++i)
         {
            MenuItem variant = pathSettings.variants[i];
            EventHandler copyRelativePath = (sender, args) => 
            { 
               string fullName = _ide.DteEnv.ActiveDocument.FullName;
               string fileName = Path.GetFileName(fullName);
               string fileNameNoExt = Path.GetFileNameWithoutExtension(fileName);
               var info = new FileInfo(fullName);
               string fileDir = info.DirectoryName;
               string relativeFile = "";
               if(!fileDir.Equals(srcPath))
               {
                  relativeFile = fileDir.Replace(srcPath + "\\", "");
               }
               
               var command = new StringBuilder(variant.command)
                  .Replace("$proj", solutionPath)
                  .Replace("$src", srcPath)
                  .Replace("$top", relativeFile)
                  .Replace("$file", fileName)
                  .Replace("$fileNoExt", fileNameNoExt)
                  .Replace("$env", pathSettings.environment);

               runCommandType(command.ToString());
            };
            addCommand(copyRelativePath, variant.display, i);
         }
      }

      // use call to delay expansion and ^ to get the new value eg. set FOO=bar & call echo %^FOO%
      // or use setlocal EnableDelayedExpansion
      private void runCommandType(string command)
      {
         if(command.Length < 5)
         {
            _ide.printLine("Invalid command.");
            return;
         }
         string commandType = command.Substring(0, 5);
         string runCommand = command.Remove(0, 5);
         if(commandType == "@copy")
         {
            System.Windows.Clipboard.SetText(runCommand);
         }
         else if(commandType == "@runi")
         {
            Process.runInteractiveCommand(runCommand, _ide.loadedSolutionPath(), _ide.printLine);
         }
         else if(commandType == "@runp")
         {
            Process.runAsyncCommand(runCommand, _ide.loadedSolutionPath(), _ide.printCmdLine);
         }
      }

      private void addCommand(EventHandler action, string text, int commandOffset)
      {
         var cmd = new CommandID(CommandSet, BaseCommandId + commandOffset);
         var menu = new OleMenuCommand(action, cmd, text);
         _commands.Add(menu);
         _ide.MenuCommandService.AddCommand(menu);
         menu.Visible = true;
      }
   }
}
