﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.ComponentModel.Design;

namespace gitproj
{
   internal sealed class RefreshProjectCommand
   {
      public const int CommandId = 4130;
      public static readonly Guid CommandSet = new Guid("bdc6171a-19f1-42c8-a5d0-d53c9cb817ab");

      private readonly IdeCore _ide;
      private GitOperations _git;
      private MenuCommand _command;

      public RefreshProjectCommand(IdeCore ide, GitOperations git)
      {
         _ide = ide;
         _git = git;
         var menuCommandID = new CommandID(CommandSet, CommandId);
         var menuItem = new MenuCommand(refreshProject, menuCommandID);
         _ide.MenuCommandService.AddCommand(menuItem);
         _command = menuItem;
      }

      public void refreshProject(object sender, EventArgs e)
      {
         _git.refreshProject();
      }
   }
}
