﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.ComponentModel.Design;
using System.Windows;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace gitproj
{
   internal sealed class CreateGitProjCommand
   {
      public const int CommandId = 4129;
      public static readonly Guid CommandSet = new Guid("0d945a41-bc80-4395-8c36-6b49a6a6e63c");
      private readonly IdeCore _ide;
      private GitOperations _git;

      public CreateGitProjCommand(IdeCore ide, GitOperations git)
      {
         _ide = ide;
         _git = git;

         var menuCommandID = new CommandID(CommandSet, CommandId);
         var menuItem = new MenuCommand(this.browseDirectory, menuCommandID);
         _ide.MenuCommandService.AddCommand(menuItem);
      }
      
      private void browseDirectory(object sender, EventArgs e)
      {
         // Requires Windows7APICodePack-Shell by Chandrakanth Tati, the other packages do not have the required strong names
         // Can alternativley use FolderBrowserDialog 
         var directoryDialog = new CommonOpenFileDialog();
         directoryDialog.Title = "Select existing git root";
         directoryDialog.IsFolderPicker = true;
         if(directoryDialog.ShowDialog() == CommonFileDialogResult.Ok)
         {
            queryFiles(directoryDialog.FileName);
         }
      }

      private void queryFiles(string projectPath)
      {
         if(!_git.isTopLevelGitPath(projectPath)){ return; }

         var solutionFiles = new SolutionFiles(projectPath);
         if(solutionFiles.anyExist())
         {
            // VsShellUtilities.ShowMessageBox causes input errors
            MessageBoxResult result = MessageBox.Show(
               "Overwrite existing project files?", "Project Files Exist", MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.OK);
            if(result == MessageBoxResult.Cancel) { return; }
         }
         _git.regenerateSolution(solutionFiles);
      }
   }
}