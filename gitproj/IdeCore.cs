﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using Microsoft.VisualStudio.Shell;
using System;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell.Interop;
using System.ComponentModel.Design;
using System.IO;

namespace gitproj
{
   class IdeCore
   {
      public Package VSPackage { get; private set; }
      public DTE2 DteEnv { get; private set; }
      public IVsOutputWindow OutputWindow { get; private set; }
      public IVsOutputWindowPane OutputPane { get; private set; }
      public IVsOutputWindowPane CmdOutputPane { get; private set; }
      public Events2 DteEvents { get; private set; }
      public SolutionEvents DteSolutionEvents { get; private set; }
      public DocumentEvents DteDocumentEvents { get; private set; }
      public ProjectItemsEvents DteProjectItemEvents { get; private set; }
      public OleMenuCommandService MenuCommandService { get; private set; }
      public IServiceProvider PackageServices { get; private set; }
      public IVsSolution Solution { get; private set; }
      public const string settingsName = "gitproj.json";

      public IdeCore(Package package)
      {
         VSPackage = package;
         PackageServices = package;
         MenuCommandService = (OleMenuCommandService)PackageServices.GetService(typeof(IMenuCommandService));
         DteEnv = (DTE2)PackageServices.GetService(typeof(DTE)); // typeof(DTE2) is null
         OutputWindow = (IVsOutputWindow)PackageServices.GetService(typeof(SVsOutputWindow));

         OutputPane = createOutputPane("gitproj");
         CmdOutputPane = createOutputPane("cmd");

         // Must reference all of these or they will get garbage collected
         DteEvents = (Events2)DteEnv.Events;
         DteProjectItemEvents = DteEvents.ProjectItemsEvents;
         DteSolutionEvents = DteEvents.SolutionEvents;
         DteDocumentEvents = DteEvents.DocumentEvents;
      }
      
      private IVsOutputWindowPane createOutputPane(string title)
      {
         Guid paneGuid = Guid.NewGuid();
         IVsOutputWindowPane pane;
         OutputWindow.CreatePane(ref paneGuid, title, 1, 0);
         OutputWindow.GetPane(ref paneGuid, out pane);
         return pane;
      }

      public void printLine(string message = "")
      {
         OutputPane.OutputString(message + "\n");
      }

      public void printCmdLine(string message = "")
      {
         CmdOutputPane.OutputString(message + "\n");
      }

      public void print(string message)
      {
         OutputPane.OutputString(message);
      }

      public Project project()
      {
         return Collections.First<Project>(DteEnv.Solution.Projects);
      }

      public string loadedSolutionPath()
      {
         if(DteEnv.Solution != null)
         {
            string path = DteEnv.Solution.FullName;
            return new FileInfo(path).DirectoryName;
         }
         else
         {
            return "";
         }
      }

      public PathSettings settings()
      {
         string solutionPath = loadedSolutionPath();
         string settingsPath = Path.Combine(solutionPath, settingsName);
         try
         {
            return PathSettings.load(settingsPath);
         }
         catch(Exception e)
         {
            printLine("Failed to parse settings: " + e.Message);
            return new PathSettings();
         }
      }
   }
}
