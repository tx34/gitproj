﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;

namespace gitproj
{
   internal class SolutionEventsHandler : IVsSolutionEvents
   {
      private IdeCore _ide;
      private IVsHierarchyEvents _itemEventsHandler;
      private uint _itemEventsCookie;

      internal SolutionEventsHandler(IdeCore ide)
      {
         _ide = ide;
         _itemEventsHandler = new ProjectEventsHandler(_ide);
      }

      public int OnAfterCloseSolution(object pUnkReserved)
      {
         _ide.printLine("IVsSolutionEvents.OnAfterCloseSolution");
         return VSConstants.S_OK;
      }

      public int OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy)
      {
         _ide.printLine("IVsSolutionEvents.OnAfterLoadProject");
         return VSConstants.S_OK;
      }

      public int OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
      {
         _ide.printLine("IVsSolutionEvents.OnAfterOpenProject");
         pHierarchy.AdviseHierarchyEvents(_itemEventsHandler, out _itemEventsCookie);
         return VSConstants.S_OK;
      }

      public int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
      {
         //fires much faster than SolutionEvents.Opened
         _ide.printLine("IVsSolutionEvents.OnAfterOpenSolution");
         return VSConstants.S_OK;
      }

      public int OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
      {
         _ide.printLine("IVsSolutionEvents.OnBeforeCloseProject");
         return VSConstants.S_OK;
      }

      public int OnBeforeCloseSolution(object pUnkReserved)
      {
         _ide.printLine("IVsSolutionEvents.OnBeforeCloseSolution");
         return VSConstants.S_OK;
      }

      public int OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy)
      {
         _ide.printLine("IVsSolutionEvents.OnBeforeUnloadProject");
         return VSConstants.S_OK;
      }

      public int OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel)
      {
         _ide.printLine("IVsSolutionEvents.OnQueryCloseProject");
         return VSConstants.S_OK;
      }

      public int OnQueryCloseSolution(object pUnkReserved, ref int pfCancel)
      {
         _ide.printLine("IVsSolutionEvents.OnQueryCloseSolution");
         return VSConstants.S_OK;
      }

      public int OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel)
      {
         _ide.printLine("IVsSolutionEvents.OnQueryUnloadProject");
         return VSConstants.S_OK;
      }
   }
}
