﻿/*
Copyright (c) 2017 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using EnvDTE;
using EnvDTE80;
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.VCProjectEngine;
using Microsoft.VisualStudio.Shell;
using System.Threading;
using System.Text.RegularExpressions;

namespace gitproj
{
   class GitOperations
   {
      private IdeCore _ide;
      private string _topLevelDirectory;
      private Process _process;
      //private IVsSolutionEvents _solutionEventsHandler;
      //private uint _solutionEventsCookie;

      public IProperty<bool> AutoStage { get; private set; }

      public enum PathFormat
      {
         RelativePath,
         AbsolutePath
      };

      public GitOperations(IdeCore ide)
      {
         _ide = ide;
         _process = new Process(_ide);
         AutoStage = new Property<bool>(false);
         //_ide.DteDocumentEvents.DocumentSaved += stageFile;
         _ide.DteSolutionEvents.Opened += updateRoot;
         _ide.DteSolutionEvents.BeforeClosing += resetRoot;
         _topLevelDirectory = "";
         //DTE does not cover all events
         //_solutionEventsHandler = new SolutionEventsHandler(_ide);
         //_ide.Solution.AdviseSolutionEvents(_solutionEventsHandler, out _solutionEventsCookie);
      }

      private void resetRoot()
      {
         _topLevelDirectory = "";
      }

      // This is a bit slow for large projects, should this be a background task instead of modal?
      public void refreshProject()
      {
         var project = Collections.First<Project>(_ide.DteEnv.Solution.Projects);
         if (project == null)
         {
            _ide.printLine("Could not refresh, no project found.");
            return;
         }
         updateFiles(project);
         project.Save();
      }

      private void updateIncludes(Project project, CommonMessagePump modalDialog)
      {
         modalDialog.TotalSteps = 0;
         modalDialog.WaitText = "Updating includes...";
         PathSettings pathSettings = _ide.settings();
         List<string> includes = _process.scrapeEnvironment(pathSettings.environment, "INCLUDE");
         includes.Insert(0, "$(VC_IncludePath)");
         includes.Insert(1, "$(WindowsSDK_IncludePath)");
         // this hangs when run in the background
         VCProject prj = project.Object as VCProject;
         IVCCollection configs = prj.Configurations;
         foreach (VCConfiguration config in configs)
         {
            var vcPlatform = config.Platform as VCPlatform;
            if (vcPlatform != null)
            {
               vcPlatform.IncludeDirectories = String.Join(";", includes);
               //_ide.printLine(config.ConfigurationName + " IncludeDirectories= " + vcPlatform.IncludeDirectories);
            }
         }
      }

      private void updateFiles(Project project)
      {
         CommonMessagePump modalDialog = createModalDialog("Refreshing project...");
         updateIncludes(project, modalDialog);
         using (var cancel = new CancellationTokenSource())
         {
            // EnvDTE is not thread safe
            var task = System.Threading.Tasks.Task.Run(() =>
            {
               Dictionary<string, ProjectItem>  staleItems = projectFiles(modalDialog, project, cancel);
               addNewFilesAndGetStaleItems(modalDialog, project, cancel, staleItems);
               removeStaleItems(modalDialog, project, cancel, staleItems);
               
            }, cancel.Token);
            modalWaitFor(modalDialog, task, cancel);
         }
         _ide.printLine("Done refresh.");
      }

      private Dictionary<string, ProjectItem> projectFiles(CommonMessagePump dialog, Project project, CancellationTokenSource cancel)
      {
         var oldFiles = new Dictionary<string, ProjectItem>();
         dialog.TotalSteps = project.ProjectItems.Count;
         dialog.WaitText = "Querying project files...";
         int i = 0;
         foreach (ProjectItem item in project.ProjectItems)
         {
            if (cancel.Token.IsCancellationRequested) { break; }
            //item.Document is null
            if (item.FileCount > 0)
            {
               dialog.CurrentStep = i++ + 1;
               string file = item.FileNames[0];
               dialog.ProgressText = file;
               oldFiles.Add(file, item);
            }
         }
         return oldFiles;
      }

      private string[] gitFiles(CommonMessagePump dialog, CancellationTokenSource cancel, PathFormat format = PathFormat.AbsolutePath)
      {
         var projectPath = _ide.loadedSolutionPath();
         dialog.TotalSteps = 0;
         dialog.WaitText = "Querying git...";
         string[] files = new string[0];
         if (!cancel.Token.IsCancellationRequested)
         {
            files = listAllFiles(projectPath, format);
         }
         return files;
      }

      private void addNewFilesAndGetStaleItems(CommonMessagePump dialog, Project project, CancellationTokenSource cancel, Dictionary<string, ProjectItem> oldFiles)
      {
         int i = 0;
         int added = 0;
         string[] files = gitFiles(dialog, cancel);
         dialog.TotalSteps = files.Length;
         dialog.WaitText = "Processing new and stale files:";
         foreach (string file in files)
         {
            if (cancel.Token.IsCancellationRequested) { break; }
            dialog.CurrentStep = i++ + 1;
            dialog.ProgressText = file;
            ProjectItem item;
            if (oldFiles.TryGetValue(file, out item))
            {
               oldFiles.Remove(file);
            }
            else
            {
               project.ProjectItems.AddFromFile(file);
               ++added;
            }
         }
         _ide.printLine(String.Format("Added {0} files.", added));
      }

      private void removeStaleItems(CommonMessagePump modalDialog, Project project, CancellationTokenSource cancel, Dictionary<string, ProjectItem> oldFiles)
      {
         int i = 0;
         modalDialog.TotalSteps = oldFiles.Count;
         modalDialog.WaitText = "Removing:";
         foreach (KeyValuePair<string, ProjectItem> file in oldFiles)
         {
            if (cancel.Token.IsCancellationRequested) { break; }
            modalDialog.CurrentStep = i++ + 1;
            modalDialog.ProgressText = file.Key;
            file.Value.Remove();
         }
         _ide.printLine(String.Format("Removed {0} files.", oldFiles.Count));
      }

      public string[] listAllFiles(string projectPath, PathFormat format = PathFormat.AbsolutePath)
      { 
         string filesOutput = _process.readOutput(Process.runBlockingCommand("git ls-files", projectPath));
         HashSet<string> files = Collections.splitNewline(filesOutput); 
         string submoduleFiles = _process.readOutput(Process.runBlockingCommand("git submodule foreach --recursive git ls-files", projectPath));
         files.UnionWith(processSubmodulePaths(submoduleFiles));
         string deletedOutput = _process.readOutput(Process.runBlockingCommand("git ls-files --deleted", projectPath));
         HashSet<string> deleted = Collections.splitNewline(deletedOutput);
         string submoduleDeleted = _process.readOutput(Process.runBlockingCommand("git submodule foreach --recursive git ls-files --deleted", projectPath));
         deleted.UnionWith(processSubmodulePaths(submoduleDeleted));
         files.ExceptWith(deleted);
         string[] fullFiles = new string[files.Count];
         int i = 0;
         foreach(string file in files)
         {
            if(format == PathFormat.RelativePath)
            {
               fullFiles[i++] = file.Replace('/', '\\');
            }
            else
            {
               fullFiles[i++] = Path.Combine(projectPath, file.Replace('/', '\\'));
            }
         }
         return fullFiles;
      }
      
      private HashSet<string> processSubmodulePaths(string submoduleFiles)
      {
         string submodulePath = "";
         var paths = Collections.splitNewline(submoduleFiles, (entry) => 
         { 
            MatchCollection matches = Regex.Matches(entry, "^Entering '(.*)'$");
            foreach(Match match in matches)
            {
               submodulePath = match.Groups[1].Value;
               return false;
            }
            return true;
         }, 
         (entry) => 
         { 
            return Path.Combine(submodulePath, entry); 
         });
         return paths;
      }

      private void updateRoot()
      {
         string projectPath = _ide.loadedSolutionPath();
         _topLevelDirectory = topLevelGitDir(projectPath);
         _ide.printLine("Solution loaded git top level: " + _topLevelDirectory);
      }

      public void regenerateSolution(SolutionFiles solutionFiles)
      {
         _ide.DteEnv.Solution.Close();
         solutionFiles.deleteFiles();
         createProject(solutionFiles);
      }

      public void createProject(SolutionFiles solutionFiles)
      {
         _ide.DteEnv.Solution.Create(solutionFiles.ProjectPath, solutionFiles.ProjectDir.Name);
         Solution2 solution = (Solution2)_ide.DteEnv.Solution;
         string templatePath = solution.GetProjectTemplate("EmptyCppProject.zip", "VC");
         _ide.printLine("Initializing project from " + templatePath);
         solution.AddFromTemplate(templatePath, solutionFiles.ProjectPath, solutionFiles.ProjectDir.Name + ".vcxproj", false); // returns null
         // if the solution is not saved immediately Solution.FullName can be empty on SolutionEvents.Opened
         solution.SaveAs(Path.Combine(solutionFiles.ProjectPath, solutionFiles.ProjectDir.Name + ".sln"));
         string n = solution.FullName;
         _ide.settings().save(Path.Combine(solutionFiles.ProjectPath, IdeCore.settingsName));   
         var project = Collections.First<Project>(solution.Projects);
         project.Save();
         CommonMessagePump modalDialog = createModalDialog("Generating project...");
         updateIncludes(project, modalDialog);
         project.Save();
         using (var cancel = new CancellationTokenSource())
         {
            // EnvDTE is not thread safe
            var addFilesTask = System.Threading.Tasks.Task.Run(() => 
            { 
               addNewFiles(modalDialog, project, cancel); 
            }, cancel.Token);
            modalWaitFor(modalDialog, addFilesTask, cancel);
         }
         solution.Close();
         solution.Open(n);
         _ide.printLine("Done generation.");
      }

/*
EnvDTE.DTE dte = ...;
string solutionName = Path.GetFileNameWithoutExtension(dte.Solution.FullName);
string projectName = project.Name;
dte.Windows.Item(EnvDTE.Constants.vsWindowKindSolutionExplorer).Activate();
((DTE2)dte).ToolWindows.SolutionExplorer.GetItem(solutionName + @"\" + projectName).Select(vsUISelectionType.vsUISelectionTypeSelect);
dte.ExecuteCommand("Project.UnloadProject");
dte.ExecuteCommand("Project.ReloadProject");
*/
      private void addNewFiles(CommonMessagePump modalDialog, Project project, CancellationTokenSource cancel)
      {
         
         string[] files = gitFiles(modalDialog, cancel, PathFormat.RelativePath);
         modalDialog.WaitText = "Adding:";
         modalDialog.TotalSteps = files.Length;

         var vcxproj = new ProjectXml(project.FullName);
         int i = 0;
         foreach (string file in files)
         {
            if (cancel.Token.IsCancellationRequested) { break; }
            modalDialog.CurrentStep = i + 1;
            modalDialog.ProgressText = file;
            // this is very slow for large numbers of files (>1000)
            //project.ProjectItems.AddFromFile(file);
            vcxproj.addFromFile(file);
            ++i;
            //prj.ProjectItems.AddFolder(dir); not implemented
            // Use "Solution Explorer > Show All Files" instead
         }
         vcxproj.save();
         _ide.printLine(String.Format("Added {0} files.", i));
      }

      private CommonMessagePump createModalDialog(string message)
      {
         var modalDialog = new CommonMessagePump();
         modalDialog.WaitTitle = message;
         modalDialog.StatusBarText = message;
         modalDialog.AllowCancel = true;
         _ide.printLine(message);
         return modalDialog;
      }

      private void modalWaitFor(CommonMessagePump modalDialog, IAsyncResult task, CancellationTokenSource cancel)
      {
         var exitCode = modalDialog.ModalWaitForHandles(task.AsyncWaitHandle);
         if (exitCode == CommonMessagePumpExitCode.UserCanceled || exitCode == CommonMessagePumpExitCode.ApplicationExit)
         {
            modalDialog = new CommonMessagePump();
            modalDialog.AllowCancel = false;
            modalDialog.TotalSteps = 0;
            string message = "Cancelling...";
            modalDialog.WaitText = message;
            modalDialog.StatusBarText = message;

            cancel.Cancel();
            // Wait for the async operation to actually cancel.
            modalDialog.ModalWaitForHandles(task.AsyncWaitHandle);
         }
      }

      private void printProgress(int index, int length, char status, string file)
      {
         _ide.printLine(String.Format("{0}/{1} {2} {3}", index, length, status, file));
      }

      private void removeFileFromIndex(ProjectItem item)
      {
         // has no document
         if(AutoStage.Value && isGitProj() && item.FileCount > 0)
         {
            _ide.printLine("Removing from git: " + item.FileNames[0]);
            ProcessOutput process = Process.runBlockingCommand("git rm --cached " + item.FileNames[0], _topLevelDirectory);
            _process.printError(process);
         }
      }

      private void stageFile(ProjectItem item)
      {
         stageFile(item.FileNames[0]);
      }

      private void stageFile(ProjectItem item, string oldName)
      {
         stageFile(item.FileNames[0]);
      }
      
      private void stageFile(Document doc)
      {
         bool includedInProject = doc.ProjectItem != null;
         if(includedInProject)
         {
            stageFile(doc.FullName);
         }
      }
      
      private void stageFile(string fullPath)
      {
         if(AutoStage.Value && isGitProj())
         {
            _ide.printLine("Staging: " + fullPath);
            ProcessOutput process = Process.runBlockingCommand("git add " + fullPath, _topLevelDirectory);
            _process.printError(process);
         }
      }
      
      private bool isGitProj()
      {
         return _topLevelDirectory != "";
      }

      public bool isTopLevelGitPath(string projectPath)
      {
         var projectDirectoryInfo = new DirectoryInfo(projectPath);
         var gitDirectoryInfo = new DirectoryInfo(topLevelGitDir(projectPath));
         if(!PathInfo.Equals(projectDirectoryInfo, gitDirectoryInfo))
         {
            _ide.printLine(projectPath + " is not a top level git directory.");
            _ide.printLine("Detected top level: " + gitDirectoryInfo.FullName);
            return false;
         }
         return true;
      }

      public string topLevelGitDir(string queryPath)
      {
         ProcessOutput gitCommand = Process.runBlockingCommand("git rev-parse --show-toplevel", queryPath);
         return _process.readOutput(gitCommand);
      }
   }
}
