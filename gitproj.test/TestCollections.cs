﻿using NUnit.Framework;
using System.Collections.Generic;

namespace gitproj.test
{
   [TestFixture]
   public class TestCollections
    {
       [TestCase("", new string[]{})]
       [TestCase("\n", new string[]{})]
       [TestCase("\n\n", new string[]{})]
       [TestCase("a", new string[]{"a"})]
       [TestCase("a\n", new string[]{"a"})]
       [TestCase("\na", new string[]{"a"})]
       [TestCase("\na\n", new string[]{"a"})]
       [TestCase("hello", new string[]{"hello"})]
       [TestCase("hello\n", new string[]{"hello"})]
       [TestCase("hello\n\n", new string[]{"hello"})]
       [TestCase("\nhello", new string[]{"hello"})]
       [TestCase("\n\nhello", new string[]{"hello"})]
       [TestCase("\nhello\n", new string[]{"hello"})]
       [TestCase("hello\ngoodbye", new string[]{"hello", "goodbye"})]
       [TestCase("hello\n\ngoodbye", new string[]{"hello", "goodbye"})]
       [TestCase("\nhello\ngoodbye\n", new string[]{"hello", "goodbye"})]
       public void canSplitNewlines(string input, string[] expected)
       {
           HashSet<string> split = Collections.splitNewline(input); 
           Assert.That(split, Is.EqualTo(new HashSet<string>(expected)));
       }
    }
}
